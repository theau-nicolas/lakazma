import { extendTheme } from '@chakra-ui/react'
import "@fontsource/gowun-batang"
import "@fontsource/poppins"


const fonts = {
    heading: `"Poppins", sans-serif;`,
    body: `"Gowun Batang", serif;`,
}

const styles = {
    global: (props) => ({
        body: {

        }
    })
}

const config = {
    initialColorMode: 'light',
    useSystemColorMode: false,
}

const colors = {
    brand: { 500: "#EED0A8", 300: "#ead6d4", 200: "#84ADEB", 100: "#CEDFF9", 700: "#292019" },
    title: { 500: "#4BE05A" },
    second: { 500: "#ead6d4" }
}

const theme = extendTheme({ config, fonts, styles, colors })
export default theme