import { Box, Center, Heading, Image, useMediaQuery } from "@chakra-ui/react";
import Slider from "react-slick";
import { motion } from "framer-motion";


export default function Brands() {

    const [isMediumDevice] = useMediaQuery('(max-width: 768px)')

    const settings = {
        arrows: false,
        infinite: true,
        autoplay: true,
        speed: 500,
        autoplaySpeed: 5000,
        slidesToShow: isMediumDevice ? 1 : 4,
    };

    const sizeY = 40
    const sizeX = 60

    return (
        <Box pt={24} pb={24} >
            <Center>
                <Heading textAlign="center" fontWeight="light" p={12} >Nos marques partenaires</Heading>
            </Center>
            <Slider style={{ zIndex: 0 }} {...settings}  >
                <Image alt="raymarine logo" src="/brands/RaymarineLogo.png" h={sizeY} p={6} minW={sizeX} />
                <Image alt="furuno logo" src="/brands/furunoLogo.png" h={sizeY} p={6} minW={sizeX} />
                <Image alt="garmin logo" src="/brands/GarminLogo.png" h={sizeY} p={6} minW={sizeX} />
                <Image alt="B&G logo" src="/brands/bngLogo.png" h={sizeY} p={6} minW={sizeX} />
                <Image alt="Icom logo" src="/brands/icomLogo.gif" h={sizeY} p={6} minW={sizeX} />
                <Image alt="Simrad logo" src="/brands/SimradLogo.png" h={sizeY} p={6} minW={sizeX} />
            </Slider>
        </Box>
    )
}