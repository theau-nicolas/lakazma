import { Box, Button, Center, Heading, HStack, Icon, SimpleGrid, Stack, Text, useColorModeValue, VStack } from "@chakra-ui/react"
import { useTranslation } from "next-i18next"
import Image from "next/image"
import Link from "next/link"
import { useRouter } from "next/router"
import { AiOutlineArrowRight } from "react-icons/ai"

const IMAGES = {
    fishing: "https://images.unsplash.com/photo-1458430085161-25ad9d8960b9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80",
    yachting: "https://images.unsplash.com/photo-1508671318294-1afb20379417?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1074&q=80",
    commercial: "https://images.unsplash.com/photo-1458430085161-25ad9d8960b9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80",
}

export default function Universes() {
    //translation
    const { t } = useTranslation("universes")

    return (
        <>
            <Center>
                <Heading fontWeight="light" p={12} >Nos domaines d{"'"}expertise</Heading>
            </Center>
            {/* <VStack gap={12} p={12} > */}
            <SimpleGrid columns={{ base: 1, lg: 3 }} p={{ base: 3, sm: 12 }} gap={12} >
                <Center>
                    <Card
                        title={t("fishing.title").toUpperCase()}
                        img={IMAGES.fishing}
                        desc={t("fishing.description")}
                        href="/fishing" />
                </Center>
                <Center>
                    <Card
                        title={t("yachting.title").toUpperCase()}
                        img={IMAGES.yachting}
                        desc={t("yachting.description")}
                        href="/yachting" />
                </Center>
                <Center>
                    <Card
                        title={t("commercial.title").toUpperCase()}
                        img={IMAGES.commercial}
                        desc={t("commercial.description")}
                        href="/commercial" />
                </Center>

            </SimpleGrid>
        </>
    )
}



const Card = ({ title, img, desc, href }) => {

    const router = useRouter()


    return (
        <Box
            maxW={'445px'}
            h="415px"
            w={'full'}
            bg={useColorModeValue('white', 'gray.900')}
            boxShadow={'2xl'}
            rounded={'md'}
            p={6}
            overflow={'hidden'}>
            <Box
                h={'210px'}
                bg={'gray.100'}
                mt={-6}
                mx={-6}
                mb={6}
                pos={'relative'}>
                <Image layout="fill" alt="marine universes" src={img} />
            </Box>
            <Stack>
                <Heading
                    color={useColorModeValue('gray.700', 'white')}
                    fontSize={'2xl'}
                    fontFamily={'body'}>
                    {title}
                </Heading>
                <Text color={'gray.500'}>
                    {desc}
                </Text>
                <Link href={href}>
                    <Button variant="ghost" >
                        <Text
                            color={'brand.500'}
                            textTransform={'uppercase'}
                            fontWeight={800}
                            fontSize={'sm'}
                            letterSpacing={1.1}>
                            Décourvir <Icon size="20" as={AiOutlineArrowRight} />
                        </Text>
                    </Button>
                </Link>
            </Stack>
        </Box>
    )
}