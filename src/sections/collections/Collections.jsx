import Card from "@/components/content/card";
import { InWrapper } from "@/components/wrappers/Wrapper";
import { Box, HStack, SimpleGrid } from "@chakra-ui/react";

export default function Collections() {
    return (
        <Box pt={24} pb={24} >
            <SimpleGrid columns={{ base: 1, md: 3 }} >
                <Card title={"bracelet"} price={30} />
                <Card title={"bracelet"} price={30} delay={0.1} />
                <Card title={"bracelet"} price={30} delay={0.2} />
            </SimpleGrid>
        </Box>
    )
}