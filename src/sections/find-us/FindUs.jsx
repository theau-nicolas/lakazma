import { AspectRatio, Box, Container, Heading } from "@chakra-ui/react";

export default function FindUs() {
    return (
        <Container maxW="full" p={0} >
            <Heading textAlign="center" fontWeight="light" p={12} >Nous trouver</Heading>
            <Box w="full" h="300px" mb={0} >

                <iframe
                    width="100%"
                    height="300px"
                    src="https://maps.google.com/maps?width=100%25&amp;height=300&amp;hl=en&amp;q=Anse%20noire+(Lakazma)&amp;t=&amp;z=16&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"
                />



            </Box>
        </Container>
    )
}