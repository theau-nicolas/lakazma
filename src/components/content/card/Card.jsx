import { InWrapper } from "@/components/wrappers/Wrapper";
import { motion } from "framer-motion";
import { SimpleGrid, Box, Text, VStack, Image, Stat, StatNumber, Heading } from "@chakra-ui/react";
import Link from "next/link";

const defaultHref = "https://www.etsy.com/fr/"


export default function Card({ title, images, price, delay, href = defaultHref }) {
    return (
        <InWrapper delay={delay} >
            <VStack p="10%" >
                <Link href={href}>
                    <Box overflow="hidden" cursor="pointer" >
                        <motion.div
                            whileHover={{ scale: [1, 1.1] }}
                            transition={{ duration: 0.3 }}
                        >
                            <Image w={"100%"} layout='fill' alt="atelier" src="/images/sac.jpeg" />
                        </motion.div>
                    </Box>
                </Link>
                <Heading fontSize="20px" >
                    {title}
                </Heading>
                <Stat>
                    <StatNumber fontSize="20px">
                        {price}€
                    </StatNumber>
                </Stat>
            </VStack>
        </InWrapper>
    )
}