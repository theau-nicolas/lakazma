import { InWrapper } from "@/components/wrappers/Wrapper";
import { Box, Heading, SimpleGrid, VStack, Text, Container } from "@chakra-ui/react";
import Image from "next/image";

const defaultSrc = "https://images.unsplash.com/photo-1656772490150-665cc9721294?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1074&q=80"

export default function Paragraph({ title, subTitle, src = defaultSrc, alt, reversed, children }) {
    return (
        <InWrapper>

            <Container maxW="4xl" p={12} mt={6} >

                <Heading textTransform={'uppercase'} textAlign={reversed && "right"}>
                    {title}
                </Heading>
                <Heading
                    textAlign={reversed && "right"}
                    color={'brand.500'}
                    textTransform={'uppercase'}
                    fontWeight={800}
                    fontSize={'md'}
                    letterSpacing={1.1}>
                    {subTitle}
                </Heading>
                <SimpleGrid columns={{ base: 1, sm: 2 }} mt={5}  >
                    {!reversed && <VStack gap={2} pr={12} >
                        {children}
                    </VStack>}
                    <Box minH="300px" overflow="hidden" rounded="2xl" position="relative" w="full" h={{ base: "200px", sm: "full" }} mt={{ base: !reversed && 12, sm: 0 }} >
                        <Image layout='fill' alt="atelier" src={src} />
                    </Box>
                    {reversed && <VStack gap={2} pl={12} textAlign="right" mt={{ base: 12, sm: 0 }} >
                        {children}
                    </VStack>}

                </SimpleGrid>

            </Container>
        </InWrapper>

    )
}