import { useContext } from "react";
import { Box, Image, Container, Heading } from "@chakra-ui/react";
import Slider from "react-slick";


export default function Carousel() {



    const settings = {
        dots: true,
        arrows: false,

        infinite: true,
        autoplay: true,
        speed: 500,
        autoplaySpeed: 5000,
        slidesToShow: 1,

    };

    return (
        <Slider style={{ zIndex: 0 }} {...settings} >

        </Slider>
    )
}

const Slide = ({ key = 0, slide }) => {
    console.log(slide)
    return (
        <Box bg="blue" h="85vh" color="white" position="relative" overflow="hidden" >
            <Image src={slide.image} bgSize="cover" w="full" h="170%" alt="boats" position="absolute" top="0px" />
            <Box zIndex={9} w={{ base: "100%", sm: "100px" }} bg="rgba(153,255,255,0.3)" h={{ base: "50%", sm: "100px" }} backdropFilter="auto"
                backdropBlur="6px" >
                <Heading>
                    {slide.title}
                </Heading>
            </Box>
        </Box>
    )
}