import { Box, Button, Center, Container, Grid, GridItem, HStack, Icon, IconButton, SimpleGrid, useColorMode, useColorModeValue } from "@chakra-ui/react";
import LogoL from "src/icons/LogoL";
import Menu from "../layout/menu";
import { useMediaQuery } from '@chakra-ui/react'
import Link from "next/link";
import useScrollPosition from "@/hooks/useScrollPosition";
import { FaSun, FaMoon } from "react-icons/fa"
import { useRouter } from "next/router";

export default function Navbar({ isWhite = true }) {

    const router = useRouter()

    const gotToMain = () => {
        router.push("/")
    }

    //dimentions
    const [isMobileDevice] = useMediaQuery('(min-width: 426px)')
    const [isSmallDevice] = useMediaQuery('(min-width: 376px)')

    //color mode
    const { colorMode, toggleColorMode } = useColorMode()
    const bg = useColorModeValue('rgba(255,255,255,0.9)', 'rgba(35,35,55,0.5)')
    const linkColor = useColorModeValue("brand.500", "brand.100")
    const borderColor = useColorModeValue("rgba(0,0,0,0)", "gray.600")
    const isDarkMode = colorMode === "dark"

    //position
    const scrollPosition = useScrollPosition()
    const scroll = !isWhite || scrollPosition > 10

    return (
        <Container
            zIndex={9}
            color={scroll ? linkColor : "brand.100"}
            maxW="full"
            h="110px"
            p={10}
            pt="35px"
            position="fixed"
            backdropFilter="auto"
            backdropBlur="16px"
            transition=".4s"
            bgColor={scroll ? bg : "none"}
            borderColor={borderColor}
        >
            <Grid templateColumns={{ base: 'repeat(9, 1fr)' }} >
                <GridItem colSpan={2} >
                    <Button variant="unstyled" onClick={gotToMain} >
                        <LogoL width="200" />
                    </Button>
                </GridItem>
                <GridItem colSpan={4}>
                    <HStack fontSize="lg" fontWeight="bold" mt={2} gap={8} color={scroll && !isDarkMode ? "gray.600" : "brand.100"} display={{ base: "none", xl: "flex" }} >
                        <Link href="/" >Accueil</Link>
                        <Link href="https://www.etsy.com/fr/" >Boutique</Link>
                        <Link href="/workshop" >{"L'atelier"}</Link>
                        <Link href="/contact" >Contact</Link>
                    </HStack>
                </GridItem>
                <GridItem colSpan={1} colStart={9} >
                    <Grid h={"full"} templateColumns={{ base: 'repeat(3, 1fr)' }} pt={2} >
                        {isSmallDevice && <GridItem colStart={{ base: 0, sm: 1 }} colSpan={1}>
                        </GridItem>}
                        {isMobileDevice && <GridItem colStart={{ base: 1, xs: 2 }} colSpan={1}>
                            <Box w={10}>
                                <IconButton size="xl" onClick={toggleColorMode} variant="unstyled" icon={!isDarkMode ? <Icon as={FaSun} /> : <Icon as={FaMoon} />} />
                            </Box>
                        </GridItem>}
                        <GridItem colSpan={1} colStart={3} >
                            <Box w={8}>
                                <Menu />
                            </Box>
                        </GridItem>
                    </Grid>
                </GridItem>
            </Grid>
        </Container >
    )
}