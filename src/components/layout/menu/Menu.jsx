import {
    Button,
    Icon,
    Drawer,
    DrawerBody,
    DrawerFooter,
    DrawerHeader,
    DrawerOverlay,
    DrawerContent,
    useDisclosure,
    Container,
    VStack,
    Text,
    Box,
    Flex,
    SimpleGrid,
    Image,
    useColorMode,
    useColorModeValue,
    IconButton,
    Grid,
    GridItem,
} from "@chakra-ui/react";
import { useRef } from "react";
import { useMediaQuery } from '@chakra-ui/react'
import Link from "next/link";
import { capitalize } from "src/utils/string";
import { useRouter } from "next/router";
import { FaSun, FaMoon } from "react-icons/fa"
import { CgClose } from "react-icons/cg"
import LogoMd from "src/icons/LogoMd";



export default function Menu() {

    const router = useRouter()

    const gotToMain = () => {
        router.push("/")
    }

    //drawer
    const { isOpen, onOpen, onClose } = useDisclosure()
    const btnRef = useRef()

    //dimensions
    const [isMobileDevice] = useMediaQuery('(max-width: 425px)')
    const [isSmallDevice] = useMediaQuery('(max-width: 375px)')


    //color mode
    const { colorMode, toggleColorMode } = useColorMode()
    const bg = useColorModeValue('gray.200', '#4a5568')
    const linkColor = useColorModeValue("grey.100", "brand.100")
    const isDarkMode = colorMode === "dark"


    return (
        <>
            <Button display={{ base: "block", xl: "none" }} variant="unstyled" w="25px" h="25px" ref={btnRef} colorScheme='teal' onClick={onOpen}>
                <Flex flexDirection="column" h="25px" w="30px" justify="space-between" >
                    <Box h={0.5} bg="gray.400" />
                    <Box h={0.5} bg="gray.400" />
                    <Box h={0.5} bg="gray.400" />
                </Flex>
            </Button>

            <Drawer
                isOpen={isOpen}
                placement='right'
                onClose={onClose}
                finalFocusRef={btnRef}
                size="full"
            >
                <DrawerOverlay />
                <DrawerContent bg={bg}>
                    <DrawerHeader p={0} >
                        <Container zIndex={9} color={linkColor} maxW="full" h="110px" p={6} pt="35px" position="fixed" >

                            <Grid templateColumns={{ base: 'repeat(4, 1fr)' }} >
                                <GridItem colSpan={1} >
                                    <Button variant="unstyled" onClick={gotToMain} >
                                        <LogoMd />
                                    </Button>
                                </GridItem>

                                <GridItem colSpan={1} colStart={4} >
                                    <Grid maxW={40} h={"full"} templateColumns={{ base: 'repeat(3, 1fr)' }} pt={2} >
                                        <GridItem colStart={{ base: 1, xs: 2 }} colSpan={1}>
                                            <Box w={10}>
                                                <IconButton size="xl" onClick={toggleColorMode} variant="unstyled" icon={!isDarkMode ? <Icon as={FaSun} /> : <Icon as={FaMoon} />} />
                                            </Box>
                                        </GridItem>
                                        <GridItem colSpan={1} colStart={3} >
                                            <Box w={8}>
                                                <IconButton size="xl" onClick={onClose} variant="unstyled" icon={<Icon as={CgClose} />} />
                                            </Box>
                                        </GridItem>
                                    </Grid>
                                </GridItem>
                            </Grid>
                        </Container>
                    </DrawerHeader>


                    <DrawerBody mt={20} >
                        <SimpleGrid columns={{ base: 1, md: 2 }} >
                            <VStack align="left" color={linkColor} pt={10} >
                                <Link href="/" >Accueil</Link>
                                <Link href="/products" >Boutique</Link>
                                <Link href="/workshop" >{"L'atelier"}</Link>
                                <Link href="/contact" >Contact</Link>
                            </VStack>
                            <Image src="/images/boats.jpg" alt="some boats" p={10} borderRadius="60px" display={{ base: "none", md: "block" }} />
                        </SimpleGrid>
                    </DrawerBody>

                    <DrawerFooter p={12} pb={0}  >
                        <VStack gap={0} w="full" color="gray.500"  >
                            <Text fontSize={8} textAlign="center" fontWeight="light"  >
                                © Copyright 2022 LAKAZMA.
                            </Text>
                            <Text fontSize={6} textAlign="center" fontWeight="light" mt={0} >
                                All Rights Reserved. | Company Registration No. XXXXXX | VAT Registration No. XX XX XX XX | Created by Théau NICOLAS
                            </Text>

                        </VStack>
                    </DrawerFooter>
                </DrawerContent>
            </Drawer>
        </>
    )
}