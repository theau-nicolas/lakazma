import LanguageSelect from "@/components/language-select";
import { Box, Container, HStack } from "@chakra-ui/react";
import Logo from "src/icons/LogoMd";

export default function MenuTemplate() {
    return (
        <>
            <Container color="brand.500" maxW="full" h="100px" pt="35px" bg="gray.100" position="fixed" >
                <HStack>
                    <Logo />
                    <LanguageSelect />
                </HStack>
            </Container>
            <Box h="100px" />
        </>
    )
}