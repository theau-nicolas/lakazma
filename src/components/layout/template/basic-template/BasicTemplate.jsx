import Header from "@/components/layout/header"
import Footer from "@/components/layout/footer"
import NavBar from "@/components/nav-bar"
import FindUs from "src/sections/find-us"

export default function BasicTemplate({ name, children }) {
    return (
        <>
            <NavBar />
            <main>
                {children}
                {/* <FindUs /> */}
            </main>
            <Footer />
        </>
    )
}