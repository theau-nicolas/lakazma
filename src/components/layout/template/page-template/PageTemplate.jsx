import Footer from "@/components/layout/footer"
import NavBar from "@/components/nav-bar"
import {
    Box,
    Center,
    Heading,
    Breadcrumb,
    BreadcrumbItem,
    BreadcrumbLink,
    BreadcrumbSeparator,
    Container,
} from "@chakra-ui/react"
import Image from "next/image"
import FindUs from "src/sections/find-us"

export default function PageTemplate({ title, dark, img, imgDesc, link, children }) {
    return (
        <>
            <NavBar isWhite={!!imgDesc} />
            <main>
                {imgDesc && <Box w="full" h="300px" position="relative" >
                    <Heading
                        fontSize={{ base: "2xl", sm: "6xl" }}
                        textTransform={'uppercase'}
                        fontWeight={800}
                        letterSpacing={1.1}
                        w="full"
                        textAlign="center"
                        position="absolute"
                        zIndex={1}
                        top={36}
                        color={dark ? "brand.500" : "white"} >
                        {title}
                    </Heading>
                    <Image src={img} layout="fill" alt={`header ${imgDesc}`} />
                </Box>}
                <Container p={12} maxW="4xl" pt={!imgDesc ? 40 : 12} >
                    <Breadcrumb >
                        <BreadcrumbItem>
                            <BreadcrumbLink href='/'>Accueil</BreadcrumbLink>
                        </BreadcrumbItem>

                        {!!link && <BreadcrumbItem>
                            <BreadcrumbLink href={`${link}`}>
                                {link}
                            </BreadcrumbLink>
                        </BreadcrumbItem>}
                    </Breadcrumb>
                </Container>
                {children}
                <FindUs />
            </main>
            <Footer />
        </>
    )
}