import NavBar from "@/components/nav-bar";
import Head from "next/head";

export default function Header({ name }) {
    return (
        <>
            <Head >
                <title></title>
                <meta name="description" content={"diginav website - marine electronic devices seller"} />
                <link rel="icon" href="/LogoMobile.svg" />

            </Head>
            <NavBar />
        </>
    )
}