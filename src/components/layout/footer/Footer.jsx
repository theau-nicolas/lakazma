import { Container, Center, Heading, List, ListItem, ListIcon, VStack, Text, SimpleGrid, HStack, useColorMode, Stack } from "@chakra-ui/react";
import Link from "next/link";
import Logo from "src/icons/LogoL";
import { capitalize } from "src/utils/string";


export default function Footer() {
    const { colorMode } = useColorMode()

    const isDarkMode = colorMode === "dark"


    return (
        <Container maxW="full" bg={!isDarkMode ? "brand.300" : "brand.700"} color="brand.700" p={12} >
            <SimpleGrid>
                <Center>
                    <Logo />
                </Center>
                <Center>
                    <HStack fontSize="lg" fontWeight="bold" mt={2} gap={8} color={!isDarkMode ? "gray.600" : "brand.100"} display={{ base: "none", xl: "flex" }} >
                        <Link href="/" >Accueil</Link>
                        <Link href="/products" >Boutique</Link>
                        <Link href="/workshop" >{"L'atelier"}</Link>
                        <Link href="/contact" >Contact</Link>
                    </HStack>
                </Center>
            </SimpleGrid>
        </Container >
    )
}