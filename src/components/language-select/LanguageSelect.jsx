import { Box, HStack, Select, Text, useMediaQuery } from "@chakra-ui/react";
import { useRouter } from "next/router";
import AmercianFlag from "src/icons/flags/American";
import FranceFlag from "src/icons/flags/France";

export default function LanguageSelect({ size = "xl" }) {
    const router = useRouter()

    const handleLocaleChange = (event) => {
        const value = event.target.value;

        router.push(router.route, router.asPath, {
            locale: value,
        });
    };

    return (
        <HStack alignItems="center" w={20}   >
            {router.locale === "fr" ? <FranceFlag width={4} height={3} /> : <AmercianFlag width={4} height={3} />}
            <Select size={size} onChange={handleLocaleChange} value={router.locale} variant="unstyled" maxW="50px"  >
                <option value="fr" >FR</option>
                <option value="en" >EN</option>
            </Select>
        </HStack>
    )
}