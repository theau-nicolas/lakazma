import { motion } from "framer-motion";

export function InWrapper({ delay, children }) {
    return (
        <motion.div
            initial={{ opacity: 0, x: -10 }}
            whileInView={{ opacity: 1, x: 0 }}
            transition={{ duration: .8, delay }}
        >
            {children}
        </motion.div>
    )
}