import { Icon } from "@chakra-ui/react";

const LogoS = ({ height = "35", width = "20", ...props }) => (
    <Icon width={width} height={height} viewBox={`0 0 ${width} ${height}`} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
        <path d="M12.4622 6.00586L19.291 25H15.0474C13.5922 24.3571 10.4282 21.8848 9.41366 17.1387C8.3991 12.3926 11.0233 7.73926 12.4622 6.00586Z" fill="currentColor" />
        <path d="M10.3648 21.6309C5.91634 15.3223 9.68193 7.91829 12.1208 5.00488L10.3648 0L2.68242 21.6309H10.3648Z" fill="currentColor" />
        <path d="M13.6085 25C12.2622 24.1992 11.2265 22.9411 10.877 22.4121H2.36537L-0.00031662 25H13.6085Z" fill="currentColor" />
    </Icon>
)

export default LogoS