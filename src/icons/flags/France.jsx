const { Icon } = require("@chakra-ui/react")

const FranceFlag = ({ width = 6, height = 4, ...props }) => {
    return (
        <Icon width={width} height={height} viewBox={`0 0 ${width} ${height}`} fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect width={width / 3} height={height} fill="#0066FF" />
            <rect x={(width / 3)} width={width / 3} height={height} fill="white" />
            <rect x={(width / 3) * 2} width={width / 3} height={height} fill="#CB0000" />
        </Icon>
    )
}

export default FranceFlag