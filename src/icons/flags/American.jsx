const { Icon } = require("@chakra-ui/react")

const AmercianFlag = ({ width = 6, height = 4, ...props }) => {
    return (
        <Icon width={width} height={height} viewBox={`0 0 ${width} ${height}`} fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect width={width} height={height} fill="white" />
            <rect y="-1" width={width} height="0.4" fill="#CB0000" />
            <rect y="0.5" width={width} height="0.4" fill="#CB0000" />
            <rect y="1.5" width={width} height="0.4" fill="#CB0000" />
            <rect y="2.5" width={width} height="0.4" fill="#CB0000" />
            <rect y="3.5" width={width} height="0.4" fill="#CB0000" />
            <rect y="4" width={width} height="0.4" fill="#CB0000" />
            <rect width={width / 2} height={height / 2} fill="#0066FF" />
        </Icon>

    )
}

export default AmercianFlag