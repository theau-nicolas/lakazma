import PageTemplate from '@/components/layout/template/page-template'

import Head from 'next/head';

export default function Contact() {


    return (
        <>
            <Head>
                <title>LAKAZMA | CONTACT</title>
                <meta name="description" content={"diginav website - marine electronic devices seller"} />
                <link rel="preload" href="https://images.unsplash.com/photo-1586763209397-4bcc40152c0e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1633&q=80" />
                <link rel="icon" href="/LogoMobile.svg" />
            </Head>

            <PageTemplate dark link="contact" >

            </PageTemplate >
        </>
    )
}

