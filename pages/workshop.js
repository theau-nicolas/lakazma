import PageTemplate from '@/components/layout/template/page-template'
import { Box, Container, Heading, HStack, SimpleGrid, Text, VStack } from '@chakra-ui/react';
import Head from 'next/head';

import Paragraph from '@/components/content/paragraph';

export default function About() {

    return (
        <>
            <Head>
                <title>LAKAZMA | L{"'"}ATELIER</title>
                <meta name="description" content={"diginav website - marine electronic devices seller"} />
                <link rel="preload" href="/images/boat.jpg" />
                <link rel="preload" href="/images/atelier.jpg" />
                <link rel="preload" href="https://images.unsplash.com/photo-1656772490150-665cc9721294?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1074&q=80" />
                <link rel="icon" href="/LogoMobile.svg" />
            </Head>

            <PageTemplate link={"L'atelier"} img="/images/boat.jpg" title="L'atelier" imgDesc="Atelier lakazma" >
                <Container maxW="4xl" p={12} mt={6}>
                    <Paragraph title="title" subTitle="subtitle" >
                        <Text>
                            Lorem ipsum dolor sit amet. Et necessitatibus mollitia eum saepe velit ut iste veritatis et doloribus ipsam aut Quis fuga est enim veritatis et laborum velit. Ut natus unde ex delectus eius aut beatae omnis et nihil sint ut molestias laboriosam. Quo pariatur dolores rem dolorum facilis qui accusamus eius ad temporibus incidunt.
                        </Text>
                        <Text>
                            Sed animi dolorum et molestiae sint a magnam maxime! Et soluta incidunt sed recusandae asperiores qui deleniti officia et commodi reiciendis qui iste aliquam in recusandae numquam eum alias minima. Et mollitia aliquam vel laborum rerum qui illo dolor ut molestiae quibusdam et autem rerum aut fugit distinctio sit voluptatem tempore.
                        </Text>
                        <Text>
                            In nobis esse sed quia delectus sit corporis odio. Et explicabo quidem aut dignissimos voluptatem cum sint minus quo perferendis architecto. Qui quas excepturi qui magni deserunt et unde dolorum vel voluptatem unde eos porro totam.
                        </Text>
                    </Paragraph>
                    <Paragraph title="title" subTitle="subtitle" reversed >
                        <Text>
                            Lorem ipsum dolor sit amet. Et necessitatibus mollitia eum saepe velit ut iste veritatis et doloribus ipsam aut Quis fuga est enim veritatis et laborum velit. Ut natus unde ex delectus eius aut beatae omnis et nihil sint ut molestias laboriosam. Quo pariatur dolores rem dolorum facilis qui accusamus eius ad temporibus incidunt.
                        </Text>
                        <Text>
                            Sed animi dolorum et molestiae sint a magnam maxime! Et soluta incidunt sed recusandae asperiores qui deleniti officia et commodi reiciendis qui iste aliquam in recusandae numquam eum alias minima. Et mollitia aliquam vel laborum rerum qui illo dolor ut molestiae quibusdam et autem rerum aut fugit distinctio sit voluptatem tempore.
                        </Text>
                        <Text>
                            In nobis esse sed quia delectus sit corporis odio. Et explicabo quidem aut dignissimos voluptatem cum sint minus quo perferendis architecto. Qui quas excepturi qui magni deserunt et unde dolorum vel voluptatem unde eos porro totam.
                        </Text>
                    </Paragraph>
                    <Paragraph title="title" subTitle="subtitle" >
                        <Text>
                            Lorem ipsum dolor sit amet. Et necessitatibus mollitia eum saepe velit ut iste veritatis et doloribus ipsam aut Quis fuga est enim veritatis et laborum velit. Ut natus unde ex delectus eius aut beatae omnis et nihil sint ut molestias laboriosam. Quo pariatur dolores rem dolorum facilis qui accusamus eius ad temporibus incidunt.
                        </Text>
                        <Text>
                            Sed animi dolorum et molestiae sint a magnam maxime! Et soluta incidunt sed recusandae asperiores qui deleniti officia et commodi reiciendis qui iste aliquam in recusandae numquam eum alias minima. Et mollitia aliquam vel laborum rerum qui illo dolor ut molestiae quibusdam et autem rerum aut fugit distinctio sit voluptatem tempore.
                        </Text>
                        <Text>
                            In nobis esse sed quia delectus sit corporis odio. Et explicabo quidem aut dignissimos voluptatem cum sint minus quo perferendis architecto. Qui quas excepturi qui magni deserunt et unde dolorum vel voluptatem unde eos porro totam.
                        </Text>
                    </Paragraph>
                </Container>
            </PageTemplate >
        </>
    )
}
