import { ChakraProvider, ColorModeScript } from '@chakra-ui/react'
import theme from 'styles/theme'
import "slick-carousel/slick/slick.css";

function Lakazma({ Component, pageProps }) {

  return (
    <ChakraProvider theme={theme}>
      <ColorModeScript initialColorMode={theme.config.initialColorMode} />
      <Component {...pageProps} />
    </ChakraProvider>
  )

}

export default Lakazma
