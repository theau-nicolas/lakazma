import BasicTemplate from '@/components/layout/template/basic-template'
import { Box, Heading, Text, VStack, Button } from '@chakra-ui/react'

import Head from 'next/head';
import Image from 'next/image';
import Collections from 'src/sections/collections';
import { InWrapper } from '@/components/wrappers/Wrapper';
import Paragraph from '@/components/content/paragraph';



export default function Home() {



  return (
    <>
      <Head>
        <title>LAKAZMA</title>
        <meta name="description" content={"diginav website - marine electronic devices seller"} />
        <link rel="preload" href="https://images.unsplash.com/reserve/NFuTknHQTsOc0uHAA4E4_4968226460_33fb941a16_o.jpg?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1174&q=80" />
        <link rel="icon" href="/LogoMobile.svg" />
      </Head>

      <BasicTemplate>
        <Box w="full" h="100vh" overflow="hidden" position="relative" >
          <Image alt="some boats" layout='fill' style={{ zIndex: -1, position: "absolute", top: "0px" }} src="/images/magasin.jpeg" />
          <VStack p={40} zIndex={8} color="white">
            <InWrapper>

              <Box bg={"brand.300"} color="brand.700" p={24} >
                <Heading  >
                  Atelier Lakazma
                </Heading>
                <Text w="30%" >
                  Gravures et bijoux
                </Text>
                <Button variant="ghost" >Découvrir</Button>
              </Box>
            </InWrapper>
          </VStack>
        </Box>
        <Collections />
        <Paragraph title="title" subTitle="subtitle" >
          <Text>
            Lorem ipsum dolor sit amet. Et necessitatibus mollitia eum saepe velit ut iste veritatis et doloribus ipsam aut Quis fuga est enim veritatis et laborum velit. Ut natus unde ex delectus eius aut beatae omnis et nihil sint ut molestias laboriosam. Quo pariatur dolores rem dolorum facilis qui accusamus eius ad temporibus incidunt.
          </Text>
          <Text>
            Sed animi dolorum et molestiae sint a magnam maxime! Et soluta incidunt sed recusandae asperiores qui deleniti officia et commodi reiciendis qui iste aliquam in recusandae numquam eum alias minima. Et mollitia aliquam vel laborum rerum qui illo dolor ut molestiae quibusdam et autem rerum aut fugit distinctio sit voluptatem tempore.
          </Text>
          <Text>
            In nobis esse sed quia delectus sit corporis odio. Et explicabo quidem aut dignissimos voluptatem cum sint minus quo perferendis architecto. Qui quas excepturi qui magni deserunt et unde dolorum vel voluptatem unde eos porro totam.
          </Text>
        </Paragraph>
      </BasicTemplate >
    </>
  )
}


