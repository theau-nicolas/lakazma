/** @type {import('next').NextConfig} */

const { i18n } = require("./next-i18next.config");

const nextConfig = {
  reactStrictMode: true,
  i18n,
  env: {
    BASE_URL: process.env.NEXT_PUBLIC_PROVIDER_HOST,
  },
  images: {
    domains: ['images.unsplash.com'],
  },
}

module.exports = nextConfig

